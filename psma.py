import os

import requests
from shapely.geometry import shape

"""
An example app to query PSMA data around a giraffe project.
`runApp(inputUrl, outputUrl)` is the main function.
"""

PSMA_API_KEY = os.environ['PSMA_API_KEY']


def getBuildings(point):
    """
        download 2D building footprints within 100m of point
    """
    longitude, latitude = point
    api_key = PSMA_API_KEY
    url = "https://api.psma.com.au/beta/v1/buildings/"
    headers = {
        'Authorization': PSMA_API_KEY,
        'API-Key': PSMA_API_KEY,
        'content-type': 'application/json'
    }
    params = {
        'latLong': f'{latitude},{longitude}',
        # 100m is max allowed
        'radius': 100,
        'perPage': '100',
        'include': 'footprint2d,elevation,centroid,averageEaveHeight,maximumRoofHeight,zonings,estimatedLevels'
    }
    response = requests.request("GET", url, headers=headers, params=params)
    response.raise_for_status()
    return response.json()


def toFeatureCollection(ret):
    """
        PSMA format to geojson
    """
    return {
        'type': 'FeatureCollection',
        'features': [{
                'type': 'Feature',
                'properties': {k: v for k, v in d.items() if k != 'footprint2d'},
                'geometry': d['footprint2d']
            }
            for d in ret['data']
        ]
    }


def toMapBoxStyle(collection):
    """
        add Mapbox Style to make geojson display in 3D
    """
    return {
        'appData': {
            'mapStyle': [{
                'id': 'psma-app',
                'type': 'fill-extrusion',
                'paint': {
                    'fill-extrusion-color': '#fff',
                    'fill-extrusion-height': {
                        'type': 'identity',
                        'property': 'maximumRoofHeight'
                    },
                },
                'source': {
                    'type': 'geojson',
                    'data': collection

                }
            }]
        }
    }


def getBoundary(payload):
    """
        return project bondary as shapely object
    """
    boundaries = [
        f for f in payload['giraffeModel']['features']
        if f['properties']['type'] == "projectBoundary"
    ]
    assert len(boundaries) == 1
    projectBoundary = shape(boundaries[0]['geometry'])
    return projectBoundary



def runApp(inputUrl, outputUrl):
    """
        fetch Giraffe payload, run app and post repsonse back to giraffe
    """
    payload = requests.get(inputUrl).json()
    print(inputUrl)
    print(payload)
    projectBoundary = getBoundary(payload)
    centroid = projectBoundary.centroid
    
    buildings = getBuildings([centroid.x, centroid.y])

    collection = toFeatureCollection(buildings)
    # remove intersecting building
    collection['features'] = [f for f in collection['features'] if not projectBoundary.intersects(shape(f['geometry']))]

    mboxStyle = toMapBoxStyle(collection)

    print(mboxStyle)
    print(outputUrl)

    requests.post(outputUrl, json=mboxStyle)

