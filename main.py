import sys
import os
import base64

from flask import Flask, request
from psma import runApp

app = Flask(__name__)

"""
Slighly edited version of GCP example: https://github.com/GoogleCloudPlatform/python-docs-samples/blob/master/run/pubsub/main.py
Flask app to receive push messages from pubsub. 
"""

@app.route('/', methods=['POST'])
def index():
    envelope = request.get_json()
    if not envelope:
        msg = 'no Pub/Sub message received'
        print(f'error: {msg}')
        return f'Bad Request: {msg}', 400

    if not isinstance(envelope, dict) or 'message' not in envelope:
        msg = 'invalid Pub/Sub message format'
        print(f'error: {msg}')
        return f'Bad Request: {msg}', 400

    pubsub_message = envelope['message']

    inputUrl = pubsub_message['attributes']['input_url']
    outputUrl = pubsub_message['attributes']['output_url']
    runApp(inputUrl, outputUrl)

    # Flush the stdout to avoid log buffering.
    sys.stdout.flush()

    return ('', 204)


if __name__ == '__main__':
    PORT = int(os.getenv('PORT')) if os.getenv('PORT') else 8080

    # This is used when running locally. Gunicorn is used to run the
    # application on Cloud Run. See entrypoint in Dockerfile.
    app.run(host='127.0.0.1', port=PORT, debug=True)