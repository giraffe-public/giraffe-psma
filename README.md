# Giraffe - PSMA example app

This project is an example of 1. how to deploy a simple app in Giraffe and 2. how to query PSMA's building footprints API.
It is based on the [GCP tutorial](https://cloud.google.com/run/docs/tutorials/pubsub).

Giraffe apps run by placing messages on Google Cloud pubsub topic unique to that app. Each message has a temporary `input_url` and `output_url`. App's can run as workers by subscribing to their topic. This project shows how they can run by deploying an endpoint to which the topic will push messages. 

This app downloads PSMA context and adds it to a Giraffe project. This gets the 2D footprints with `maximumRoofHeight` for all buildings within 100m of the project centroid.


##  Deploy docker to Cloud Run (rerun this for subsequent deploys)
```
PROJECT_ID=<YOUR GOOGLE APP ENGINE PROJECT ID>
NAME=psma-app
PSMA_API_KEY=<ENV VARIABLE REQUIRED BY YOUR APP>

gcloud builds submit --tag gcr.io/$PROJECT_ID/$NAME
gcloud run deploy $NAME --image gcr.io/$PROJECT_ID/$NAME --platform managed --update-env-vars PSMA_API_KEY=$PSMA_API_KEY
```

